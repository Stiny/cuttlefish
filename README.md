# Cuttlefish (IRCD)
## Introduction
Cuttlefish is an internet relay chat daemon (IRCD) written from scratch in C++ with the purpose of being highly modular in design.
It will be built on the new C++11 standards.
